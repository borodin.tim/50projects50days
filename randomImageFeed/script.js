const container = document.querySelector('.container');
const unsplashURL = 'https://source.unsplash.com/random/';
const rows = 4;

loadImages();

function loadImages() {
    for (let i = 0; i < 3 * rows; i++) {
        const img = document.createElement('img');
        img.src = `${unsplashURL}${getRangomNum()}x${getRangomNum()}`;
        container.appendChild(img);
    }
}

function getRangomNum() {
    return 300 + Math.floor(Math.random() * 10);
}