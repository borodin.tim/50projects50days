const passwordInput = document.getElementById('password');
const backgroundDiv = document.getElementById('background');
const maxBlur = 20;
const goodPasswordLength = 12;

passwordInput.addEventListener('input', (e) => {
    let blurAmount = 0;
    let passwordStrengthLevel = getPasswordStrengthLevel(e.target.value.toString());

    let oldValue = passwordStrengthLevel;
    let oldMin = 0
    let oldMax = 100
    let newMin = maxBlur;
    let newMax = 0
    blurAmount = ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin
    // console.log('🚀 ~ blurAmount', blurAmount);
    backgroundDiv.style.filter = `blur(${blurAmount}px)`;
});

const getPasswordStrengthLevel = (password) => {
    let strength = 0;

    //1. Length
    let oldValue = password.length;
    let oldMin = 0;
    let oldMax = goodPasswordLength;
    let newMin = 0;
    let newMax = 20; // % in the final strength
    const lengthStrength = ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin;
    // console.log('length', lengthStrength);


    // 2. at leat 1 upper case letter
    oldValue = (password !== password.toLowerCase()) ? 1 : 0;
    oldMin = 0;
    oldMax = 1;
    newMin = 0;
    newMax = 16; // % in the final strength
    const upperCaseStrength = ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin;
    // console.log('upperCase', upperCaseStrength);

    // 3. At leat 2 lower case letter
    oldValue = (password !== password.toUpperCase()) ? 1 : 0;
    oldMin = 0;
    oldMax = 1;
    newMin = 0;
    newMax = 16; // % in the final strength
    const lowerCaseStrength = ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin;
    // console.log('lowerCase', lowerCaseStrength);

    // 4. At least 1 digit
    oldValue = password.match(/(.*\d)/) ? 1 : 0;
    oldMin = 0;
    oldMax = 1;
    newMin = 0;
    newMax = 16; // % in the final strength
    const digitStrength = ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin;
    // console.log('digit', digitStrength);

    // 5. At least 1 special char
    oldValue = password.match(/.[$@$!% #+=()\^?&]/) ? 1 : 0;
    oldMin = 0;
    oldMax = 1;
    newMin = 0;
    newMax = 16; // % in the final strength
    const specialCharStrength = ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin;
    // console.log('specialChar', specialCharStrength);

    // 6. All characters are the same, no matter the length
    const firstChar = password[0];
    oldValue = password.split('').reduce((total, char) => total && (firstChar === char), true) ? 0 : 1;
    oldMin = 0;
    oldMax = 1;
    newMin = 0;
    newMax = 16; // % in the final strength
    const notSameLetterStrength = ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin;
    // console.log('notSameLetter', notSameLetterStrength);

    strength = lengthStrength + upperCaseStrength + lowerCaseStrength + digitStrength + specialCharStrength + notSameLetterStrength;
    // console.log('strength:', strength);
    return strength;
}