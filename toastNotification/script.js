const button = document.getElementById('button');
const toasts = document.getElementById('toasts');

const toastTimeout = 5000;

const infoMessages = [
    'Info message One',
    'Info message Two',
    'Info message Three',
];

const successMessages = [
    'Success message One',
    'Success message Two',
    'Success message Three',
];

const errorMessages = [
    'Error message One',
    'Error message Two',
    'Error message Three',
];

const types = ['info', 'success', 'error'];

// button.addEventListener('click', () => createNotification());
// button.addEventListener('click', () => createNotification('Info message'));
// button.addEventListener('click', () => createNotification('Invalid data', 'error'));
// button.addEventListener('click', () => createNotification('Submitted successfully', 'success'));
button.addEventListener('click', () => createNotification());

function createNotification(message = null, type = null) {
    const toast = document.createElement('div');
    toast.className = 'toast';
    const typeClass = type ? type : getRandomType();
    toast.classList.add(typeClass);
    toast.innerText = message ? message : getRandomMessage(typeClass);
    toasts.appendChild(toast);

    setTimeout(() => { toast.classList.add('fade-out') }, toastTimeout - 500);
    setTimeout(() => { toast.remove() }, toastTimeout);
}

function getRandomMessage(typeClass) {
    switch (typeClass) {
        case 'info':
            return infoMessages[Math.floor(Math.random() * infoMessages.length)];
        case 'success':
            return successMessages[Math.floor(Math.random() * successMessages.length)];
        case 'error':
            return errorMessages[Math.floor(Math.random() * errorMessages.length)];
        default:
            break
    }
}

function getRandomType() {
    return types[Math.floor(Math.random() * types.length)];
}
