const addBtn = document.getElementById('add');

const notes = JSON.parse(localStorage.getItem('notes'));
if (notes) {
    notes.forEach(note => addNewNote(note));
}

addBtn.addEventListener('click', () => addNewNote());

function addNewNote(text = '') {
    const note = document.createElement('div');

    note.classList.add('note');

    note.innerHTML = `
    <div class="tools">
        <button class="edit ${text === "" ? "active" : ""}"><i class="fas fa-edit"></i></button>
        <button class="delete"><i class="fas fa-trash-alt"></i></button>
    </div>
    <div class="main ${text ? "" : "hidden"}"></div>
    <textarea class="${text ? "hidden" : ""}" autofocus></textarea>
    `;

    const editBtn = note.querySelector('.edit');
    const deleteBtn = note.querySelector('.delete');
    const main = note.querySelector('.main');
    const textArea = note.querySelector('textarea');

    textArea.value = text;
    main.innerHTML = marked(text);

    deleteBtn.addEventListener('click', () => {
        note.remove();
        updateLocalStorage();
    });

    editBtn.addEventListener('click', () => {
        toggleDisplayTextAndEdit(main, textArea, editBtn);
    });

    textArea.addEventListener('input', (e) => {
        const { value } = e.target;
        main.innerHTML = marked(value);
        updateLocalStorage();
    })

    if (!main.classList.contains('hidden')) {
        main.addEventListener('click', () => {
            toggleDisplayTextAndEdit(main, textArea, editBtn);
        });
    }

    if (!textArea.classList.contains('hidden')) {
        textArea.focus();
        textArea.setSelectionRange = 0;
    }

    document.body.appendChild(note);
}

const updateLocalStorage = () => {
    const notesText = document.querySelectorAll('textarea');
    const notes = [];
    notesText.forEach(note => notes.push(note.value));
    localStorage.setItem('notes', JSON.stringify(notes));
}


function toggleDisplayTextAndEdit(main, textArea, editBtn) {
    main.classList.toggle('hidden');
    textArea.classList.toggle('hidden');
    if (!textArea.classList.contains('hidden')) {
        textArea.focus();
    }
    editBtn.classList.toggle('active')
}