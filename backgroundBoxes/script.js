const button = document.getElementById('btn');
const boxesEl = document.getElementById('boxes');

button.addEventListener('click', () => {
    boxesEl.classList.toggle('big');
})

for (let i = 0; i < 16; i++) {
    const box = document.createElement('div');
    box.classList.add('box');

    box.style.backgroundPosition = `${-125 * (i % 4)}px ${-125 * parseInt(i / 4)}px`;

    boxesEl.appendChild(box);
}