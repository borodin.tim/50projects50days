const sliderContainer = document.querySelector('.slider-container');
const slideLeft = document.querySelector('.left-slide');
const slideRight = document.querySelector('.right-slide');
const downButton = document.querySelector('.down-button');
const upButton = document.querySelector('.up-button');
const slidesLength = slideRight.querySelectorAll('div').length;

let activeSlideIndex = 0;

slideLeft.style.top = `-${(slidesLength - 1) * 100}vh`;

downButton.addEventListener('click', () => {
    activeSlideIndex--;
    if (activeSlideIndex < 0) {
        activeSlideIndex = slidesLength - 1;
    }
    updateSlides();
});

upButton.addEventListener('click', () => {
    activeSlideIndex++;
    if (activeSlideIndex > slidesLength - 1) {
        activeSlideIndex = 0;
    }
    updateSlides();
});

function updateSlides() {
    slideRight.style.transform = `translateY(-${activeSlideIndex * 100}vh)`;
    slideLeft.style.transform = `translateY(${activeSlideIndex * 100}vh)`;
}
