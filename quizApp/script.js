const quiz = document.getElementById('quiz');
const answerEls = document.querySelectorAll('.answer');
const questionEl = document.getElementById('question');
const a_text = document.getElementById('a_text');
const b_text = document.getElementById('b_text');
const c_text = document.getElementById('c_text');
const d_text = document.getElementById('d_text');
const submitBtn = document.getElementById('submit');

let currentQuiz = 0;
let score = 0;

loadQuiz();

function loadQuiz() {
    deselectAnswers();

    const currentQUizData = quizData[currentQuiz];

    questionEl.innerText = currentQUizData.question;

    a_text.innerText = currentQUizData.a;
    b_text.innerText = currentQUizData.b;
    c_text.innerText = currentQUizData.c;
    d_text.innerText = currentQUizData.d;
}

function deselectAnswers() {
    answerEls.forEach(answerEl => answerEl.checked = false);
}

function getSelected() {
    let answer;
    answerEls.forEach(answerEl => {
        if (answerEl.checked === true) {
            answer = answerEl.id;
        }
    });
    return answer;
}

submitBtn.addEventListener('click', () => {
    submitBtnFunc();
})

function submitBtnFunc() {
    const answer = getSelected();
    if (answer) {
        if (answer === quizData[currentQuiz].correct) {
            score++;
        }
        currentQuiz++;
        if (currentQuiz < quizData.length) {
            loadQuiz();
        } else {
            quiz.innerHTML = `
                <h2> You answered at ${score}/${quizData.length} questions correctly</h2>
                <button onClick="${reload}">Reload</button>
            `;
        }
    }
}

function reload() {
    location.reload();
}

window.addEventListener('keydown', (e) => {
    let code;
    if (e.key !== undefined) {
        code = e.key;
    }

    switch (code) {
        case ' ':
        case 'Enter':
            if (quiz.innerHTML.includes('Reload')) {
                reload();
            } else {
                submitBtnFunc();
            }
            break;
        case '1':
            answerEls[0].checked = true;
            break;
        case '2':
            answerEls[1].checked = true;
            break;
        case '3':
            answerEls[2].checked = true;
            break;
        case '4':
            answerEls[3].checked = true;
            break;
    }
})