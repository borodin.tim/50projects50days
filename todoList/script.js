const todoInput = document.querySelector('.todo-input');
const todosList = document.querySelector('.todos-list');
const form = document.querySelector('#form');

document.addEventListener('DOMContentLoaded', () => {
    const todos = JSON.parse(localStorage.getItem('todo'));
    todos.forEach(todo => addNewTodo(todo));
})

form.addEventListener('submit', (e) => {
    e.preventDefault();
    addNewTodo();
})

function addNewTodo(todo) {
    let text = todoInput.value;
    let completed = false;
    if (todo) {
        text = todo.text;
        completed = todo.completed;
    }

    const div = document.createElement('div');
    div.classList.add('todo-item');
    if (completed) {
        div.classList.add('completed');
    }
    div.innerText = text;
    div.addEventListener('click', (e) => {
        div.classList.toggle('completed');
        updateLocalStorage();
    });
    div.addEventListener("contextmenu", function (e) {
        e.preventDefault();
        div.remove();
        updateLocalStorage();
    }, false);

    todosList.appendChild(div);

    todoInput.value = '';

    updateLocalStorage();
}

function updateLocalStorage() {
    const todoItems = document.querySelectorAll('.todo-item');
    const items = [];
    todoItems.forEach(item => {
        items.push({
            text: item.innerText,
            completed: item.classList.contains('completed')
        })
    })
    localStorage.setItem('todo', JSON.stringify(items));
}