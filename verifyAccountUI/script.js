const codeElements = document.querySelectorAll('.code');

codeElements[0].focus();

codeElements.forEach((codeEl, idx) => {
    codeEl.addEventListener('keydown', (e) => {
        let key = e.keyCode || e.charCode;
        const regex = new RegExp('^[0-9]$');

        if (regex.test(String.fromCharCode(e.keyCode))) {
            codeElements[idx].value = '';
            setTimeout(() => { codeElements[(idx + 1 >= codeElements.length ? codeElements.length - 1 : idx + 1)].focus() }, 10);
        } else if (key === 8 || key === 46) {
            setTimeout(() => {
                codeElements[idx].value = '';
                codeElements[(idx - 1) < 0 ? 0 : idx - 1].focus();
            }, 10);
        }
    });
});
