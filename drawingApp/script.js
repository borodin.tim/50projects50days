const canvas = document.getElementById('canvas');
const increaseBtn = document.getElementById('increase');
const decreaseBtn = document.getElementById('decrease');
const sizeEl = document.getElementById('size');
const squareBtn = document.getElementById('square');
const colorEl = document.getElementById('color');
const eraserBtn = document.getElementById('eraser');
const saveBtn = document.getElementById('save');
const clearBtn = document.getElementById('clear');

const ctx = canvas.getContext('2d');

let size = 10;
let isPressed = false;
let shape;
let isEraser = false;
let color = 'black';
let colorEraser = 'white';
let colorBeforeErase;
let x;
let y;
let x2;
let y2;

const shapesEnum = {
    square
}

canvas.addEventListener('mousedown', (e) => {
    isPressed = true;
    x = e.offsetX;
    y = e.offsetY;

    x2 = e.offsetX;
    y2 = e.offsetY;
    drawCircle(x2, y2);
    drawLine(x, y, x2, y2);
    x = x2;
    y = y2;
});

canvas.addEventListener('mouseup', (e) => {
    isPressed = false;
    x = undefined;
    y = undefined;

});

canvas.addEventListener('mousemove', (e) => {
    if (isPressed) {
        if (shape === undefined) {
            const x2 = e.offsetX;
            const y2 = e.offsetY;
            drawCircle(x2, y2);
            drawLine(x, y, x2, y2);
            x = x2;
            y = y2;
        } else {
            switch (shape) {
                case shapesEnum.square:
                    if (x2 > x && y2 > y) {
                        ctx.clearRect(x - size, y - size, (x2 - x + (size * 2)), (y2 - y + (size * 2)));
                    } else if (x2 < x && y2 < y) {
                        ctx.clearRect(x + size, y + size, (x2 - x - (size * 2)), (y2 - y - (size * 2)));
                    } else if (x2 > x && y2 < y) {
                        ctx.clearRect(x - size, y + size, (x2 - x + (size * 2)), (y2 - y - (size * 2)));
                    } else if (x2 < x && y2 > y) {
                        ctx.clearRect(x + size, y - size, (x2 - x - (size * 2)), (y2 - y + (size * 2)));
                    }
                    x2 = e.offsetX;
                    y2 = e.offsetY;
                    drawSquareShape(x, y, x2, y2);
                    break;
                default:
                    break;
            }
        }
    }
});

function drawSquareShape(x1, y1, x2, y2) {
    ctx.strokeStyle = color;
    ctx.lineWidth = size * 2;
    ctx.strokeRect(x1, y1, x2 - x1, y2 - y1);
}

function drawCircle(x, y) {
    ctx.beginPath();
    ctx.arc(x, y, size, 0, Math.PI * 2);
    ctx.fillStyle = color;
    ctx.fill();
}

function drawLine(x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = color;
    ctx.lineWidth = size * 2;
    ctx.stroke();
}


function updateSizeOnScreen() {
    sizeEl.innerText = size;
}

colorEl.addEventListener('change', (e) => color = e.target.value);

increaseBtn.addEventListener('click', () => {
    size += 5;
    if (size > 50) {
        size = 50;
    }
    updateSizeOnScreen();
});

decreaseBtn.addEventListener('click', () => {
    size -= 5;
    if (size < 5) {
        size = 5;
    }
    updateSizeOnScreen();
});

squareBtn.addEventListener('click', () => {
    if (shape === shapesEnum.square) {
        shape = undefined;
    } else {
        shape = shapesEnum.square;
    }
});

saveBtn.addEventListener('click', () => {
    var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    window.location.href = image; // it will save locally

    // var dataURL = canvas.toDataURL("image/jpeg", 1.0);
    // downloadImage(dataURL, 'image.jpeg');
});

// function downloadImage(data, filename = 'untitled.jpeg') {
//     var a = document.createElement('a');
//     a.href = data;
//     a.download = filename;
//     document.body.appendChild(a);
//     a.click();
// }

eraserBtn.addEventListener('click', () => {
    isEraser = !isEraser;
    console.log(isEraser)
    if (isEraser) {
        colorEl.disabled = true;
        eraserBtn.classList.add('active');
        colorBeforeErase = color;
        color = colorEraser;
    } else {
        colorEl.disabled = false;
        eraserBtn.classList.remove('active');
        color = colorBeforeErase;
    }
});

clearBtn.addEventListener('click', () => { ctx.clearRect(0, 0, canvas.width, canvas.height) });