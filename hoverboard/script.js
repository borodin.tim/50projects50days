const container = document.getElementById('container');
const colors = ['#00ff88', '#247ac0', '#ffef16', '#ff1616', '#e416ff', '#31ff16', '#0059ff'];
const SQUARES = 500;

for (let i = 0; i < SQUARES; i++) {
    const square = document.createElement('div');
    square.classList.add('square');

    square.addEventListener('mouseover', () => {
        const color = colors[parseInt(Math.floor(Math.random() * colors.length))];
        square.style.backgroundColor = color;
        square.style.boxShadow = `0 0 2px ${color}, 0 0 10px ${color}`;
    });

    square.addEventListener('mouseout', () => {
        square.style.backgroundColor = '#1d1d1d';
        square.style.boxShadow = '0 0 2px #000';
    });

    container.appendChild(square);
}
