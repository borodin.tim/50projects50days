const leftBtn = document.getElementById('left');
const rightBtn = document.getElementById('right');
const carousel = document.querySelector('.carousel');
const imgsEl = document.getElementById('imgs');
const images = document.querySelectorAll('#imgs img');

const intervalTime = 3000;
let imageIdx = 0;
let interval = setInterval(rotateRight, intervalTime);

leftBtn.addEventListener('click', () => {
    rotateLeft();
});

rightBtn.addEventListener('click', () => {
    rotateRight();
});

function rotateLeft() {
    imageIdx--;
    changeImage();
}

function rotateRight() {
    imageIdx++;
    changeImage();
}

function changeImage() {
    if (imageIdx < 0) {
        imageIdx = images.length - 1;
    } else if (imageIdx >= images.length) {
        imageIdx = 0;
    }
    imgsEl.style.transform = `translateX(${-imageIdx * 500}px)`;
    resetInterval();
}

function resetInterval() {
    clearInterval(interval);
    interval = setInterval(rotateRight, intervalTime);
}
