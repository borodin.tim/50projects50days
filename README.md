# 50 projects 50 days

Started: 15 December 2020

Project 1 - [Expanding Cards](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/expandingCards)

Project 2 - [Progress Steps](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/progressSteps)

Project 3 - [Rotating Navigation](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/rotatingNavigation)

Project 4 - [Hidden Search Widget](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/hiddenSearchWidget)

Project 5 - [Blurry Loading](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/blurryLoading)

Project 6 - [Scroll Animation](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/scrollAnimation)

Project 7 - [Split Landing Page](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/splitLandingPage)

Project 8 - [Form Wave Animation](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/formWaveAnimation)

Project 9 - [Sound Board](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/soundBoard)

Project 10 - [Dad Jokes](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/dadJokes)

Project 11 - [Event KeyCodes](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/eventKeyCodes)

Project 12 - [FAQ Collapse](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/faqCollapse)

Project 13 - [Random Choice Picker](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/randomChoicePicker)

Project 14 - [Animated Navigation](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/animatedNavigation)

Project 15 - [Incrementing Counter](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/incrementingCounter)

Project 16 - [Drink Water](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/drinkWAter)

Project 17 - [Movie App](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/movieApp)

- [Movie App project repository](https://gitlab.com/borodin.tim/movieapp)
- [Movie App on Heroku](http://movies-app-nodejs-dev.herokuapp.com/)

Project 18 - [Background Slider](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/backgroundSlider)

Project 19 - [Theme Clock](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/themeClock)

Project 20 - [Button Ripple Effect](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/buttonRippleEffect)

Project 21 - [Drag N Drop](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/dragNDrop)

Project 22 - [Drawing App](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/drawingApp)

- [Drawing App project repository](https://gitlab.com/borodin.tim/drawingapp)
- [Drawing App on Netlify](https://drawing-app-jscanvas.netlify.app/)

Project 23 - [Kinetic CSS Loader](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/kineticCSSLoader)

Project 24 - [Content Placeholder](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/contentPlaceholder)

Project 25 - [Sticky Navbar](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/stickyNavBar)

Project 26 - [Double Vertical Slider](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/doubleVerticalSlider)

Project 27 - [Toast Notification](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/toastNotification)

Project 28 - [Github Profiles](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/githubProfiles)

- [Github Profiles project repository](https://gitlab.com/borodin.tim/gitprofiles)
- [Github Profiles App on Netlify](https://github-gitlab-profiles.netlify.app/)

Project 29 - [Double Click Heart](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/doubleClickHeart)

Project 30 - [Auto Text Effect](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/autoTextEffect)

Project 31 - [Password Generator](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/passwordGenerator)

Project 32 - [Good Cheap Fast Checkboxes](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/goodCheapFastCheckboxes)

Project 33 - [Notes App](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/notesApp)

Project 34 - [Animated Countdown](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/animatedCountdown)

Project 35 - [Image Carousel](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/imageCarousel)

Project 36 - [Hoverboard](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/hoverboard)

Project 37 - [Pokedex](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/pokedex)

Project 38 - [Mobile Tab Navigation](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/mobileTabNavigation)

Project 39 - [Password Strength Background](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/passwordStrengthBackground)

Project 40 - [Background Boxes](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/backgroundBoxes)

Project 41 - [Verify Account UI](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/verifyAccountUI)

Project 42 - [Live User Filter](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/liveUserFilter)

Project 43 - [Feedback UI Design](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/feedbackUIDesign)

Project 44 - [Custom Range Slider](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/customRangeSlider)

Project 45 - [Netflix Navigation](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/netflixNavigation)

Project 46 - [Quiz App](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/quizApp)

Project 47 - [Testimonial Box Switcher](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/testimonialBoxSwitcher)

Project 48 - [Random Image Feed](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/randomImageFeed)

Project 49 - [Todo List](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/todoList)

Project 50 - [Insect Catch Game](https://gitlab.com/borodin.tim/50projects50days/-/tree/dev/insectCatchGame)

- [Insect Catch Game project repository](https://gitlab.com/borodin.tim/insects-catch-game)
- [Insect Catch Game on Netlify](https://insects-catch-game.netlify.app/)
