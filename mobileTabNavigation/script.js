const menus = document.querySelectorAll('nav ul li');
const images = document.querySelectorAll('.phone img');

menus.forEach((menu, idx) => {
    menu.addEventListener('click', () => {
        menus.forEach(menu => menu.classList.remove('active'));
        images.forEach(image => image.classList.remove('show'));
        menu.classList.add('active');
        images[idx].classList.add('show');
    });
});