const jokeElement = document.querySelector('.joke');
const jokeBtn = document.getElementById('jokeBtn');

jokeBtn.addEventListener('click', () => {
    generateJoke();
});

generateJoke();

async function generateJoke() {
    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    // old style - .then
    // fetch('https://icanhazdadjoke.com/', config)
    //     .then(response => response.json())
    //     .then(data => jokeElement.innerText = data.joke);

    // new style with async/await
    const response = await fetch('https://icanhazdadjoke.com/', config);
    if (!response.ok) {
        throw new Error(`HTTP error! Status ${response.status}`);
    }
    const data = await response.json();
    jokeElement.innerText = data.joke;
}
